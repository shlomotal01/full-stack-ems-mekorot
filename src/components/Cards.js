import React from 'react';
import CardItem from './CardItem';
import './Cards.css';

function Cards() {
    return (
        <div className='cards'>
            <h1> Mekorot CARDS: </h1>
            <div className="cards__container">
                <div className="cards_wrapper">
                    <ul className="cards__items">
                        <CardItem 
                        src="images/m1.jpg"
                        text="Mekorot1 kfjnwelkwkem;we"
                        label='Aventure'
                        path='/services'
                        />
                        <CardItem 
                        src="images/m2.jpg"
                        text="Mekorot2 waklwjqlqwjksefewf"
                        label='Luxury'
                        path='/services'
                        />
                    </ul>
                    {/* <ul className="cards__items">
                        <CardItem 
                        src="images/img-3.jpg"
                        text="Mekorot3"
                        label='Mystery'
                        path='/MapPage'
                        />
                        <CardItem 
                        src="images/img-4.jpg"
                        text="Mekorot4"
                        label='Luxury'
                        path='/machines'
                        />
                        <CardItem 
                        src="images/img-8.jpg"
                        text="Mekorot5"
                        label='Luxury'
                        path='/stammmmm'
                        />
                    </ul> */}
                </div>
            </div>
        </div>
    )
}

export default Cards

