import React from 'react';
import { findRenderedComponentWithType } from 'react-dom/test-utils';
import Navbar from './components/Navbar';
import {BrowserRouter as Router, Switch , Route } from 'react-router-dom';
import './App.css';
import Home from './components/pages/Home';
import Services from './components/pages/Services';
import MapPage from './components/pages/MapPage';
import SignUp from './components/pages/SignUp';





function App() {
  return (
    <>
     <Router>
      <Navbar/>
      <Switch>
        <Route path='/' exact component={Home}/>
        <Route path='/services' component={Services} />
        <Route path='/MapPage' component={MapPage} />
        <Route path='/sign-up' component={SignUp} />
      </Switch>
      </Router>
    </>
  );
}

export default App;
